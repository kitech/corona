# Package

version       = "0.1.0"
author        = "egitop"
description   = "A new awesome nimble package"
license       = "GPL-3.0"
srcDir        = "src"


# Dependencies

requires "nim >= 0.19.6"
