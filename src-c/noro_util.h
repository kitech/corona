#ifndef _NORO_UTIL_H_
#define _NORO_UTIL_H_

#include <unistd.h>

pid_t gettid();

int (array_randcmp) (const void*a, const void*b);

#endif

