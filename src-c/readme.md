A library that implemention golang's core features,
goroutines, schedulers, channels and garbage collect.

### Features

* [x] stackful coroutine
* [x] multiple threads
* [x] channels
* [ ] golang select semantic
* [x] garbage collect
* [x] syscall hook
* [ ] explict syscall functions wrapper

### Thirdpartys

Thanks all the contributors.

* libcoro 
* libchan https://github.com/tylertreat/chan
* libcollectc
* rixlog
* libgc >= 8.2.0
* libevent >= 2.1
